/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.jzab.edu.concurrency;

/**
 *
 * @author zjaramil
 */
public class Coordination {

  private volatile  boolean ready;

  public synchronized void orderFood(){
    System.out.println("Ordering food");
    while(!ready){
      System.out.println("Waiting for food");
      try {
        wait();
      }
      catch (InterruptedException ex) {
      }
    }
    System.err.println("Food is here");
}

  public synchronized void prepareFood(){
    System.out.println("Preparing food");
    try {
      Thread.sleep(1000);
      ready = true;
      notifyAll();
    }
    catch (InterruptedException ex) {
    }
  }

  public static void main(String[] args) throws InterruptedException{
    Coordination coordination = new Coordination();
    Thread t1 = new Thread(() -> {
      coordination.orderFood();
    });
    Thread t2 = new Thread(() -> {
      coordination.prepareFood();
    });
    t1.start();
    t2.start();
  }

}
