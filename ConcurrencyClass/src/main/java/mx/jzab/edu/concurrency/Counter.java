/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.jzab.edu.concurrency;

import org.apache.commons.lang.time.StopWatch;

/**
 *
 * @author zjaramil
 */
public class Counter {

  private int count;
  private int count2;

  private Object lock1 = new Object();
  private Object lock2 = new Object();

  public void increment(){
    synchronized (lock1){
      count++;
    }
  }

  public void decrement(){
    synchronized (lock1){
      count++;
    }
  }

  public int getCount(){
    synchronized (lock1){
      return count;
    }
  }

  public void increment2(){
    synchronized (lock2){
      count2++;
    }
  }

  public void decrement2(){
    synchronized (lock2){
      count2--;
    }
  }

  public int getCount2(){
    synchronized (lock2){
      return count2;
    }
  }

  public void race(){
    StopWatch stopWatch = new StopWatch();
    stopWatch.start();

    long limit = Integer.MAX_VALUE / 10;

    Thread increase = new Thread(() -> {
      for (long i = 0; i < limit; i++){
        increment();
      }
    });

    Thread increment2 = new Thread(() -> {
      for (long i = 0; i < limit; i++){
        increment2();
      }
    });

    Thread increment3 = new Thread(() -> {
      for (long i = 0; i < limit; i++){
        increment2();
      }
    });

    increase.start();
    increment2.start();
    increment3.start();

    try {
      increase.join();
      increment2.join();
    }
    catch (InterruptedException ex) {
    }

    System.out.println("Counter " + getCount() + " elapsed " + stopWatch.getTime());

  }

  public static void main(String[] args){
    for (int i = 0; i < 5; i++){
      new Counter().race();
    }

  }

}
