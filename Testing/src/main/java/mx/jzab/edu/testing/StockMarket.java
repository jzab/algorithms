/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.jzab.edu.testing;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author zjaramil
 */
public class StockMarket {

  public Buyer buyer;

  public boolean tryBuy(int id, String company, float oldValue) throws SQLException, FileNotFoundException, IOException{
    Properties p = new Properties();
    p.load(new FileReader("filename"));

    //Load correct url
    String url = "";
    if(p.getProperty("dev") != null){
      url = p.getProperty("dev.url");
    }
    else{
      url = p.getProperty("prod.url");
    }

    Connection c = DriverManager.getConnection(url);
    PreparedStatement ps = c.prepareStatement("select value, stock_id from stock where company = ?");
    ResultSet rs = ps.executeQuery();

    //Company exists
    if(rs.next()){
      Float value     = rs.getFloat("value");
      Integer stockId = rs.getInt("stock_id");

      if(value/oldValue > 1.15){
        System.out.println("Buy Stock");

        PreparedStatement psb = c.prepareStatement("insert into stock_items(user_id,stock_id) values(?,?)");
        psb.setInt(1, id);
        psb.setInt(2, stockId);
        boolean buy = psb.execute();
        return buy;
      }

      else if(value/oldValue > 1.25){
        System.out.println("Buy Stock");

        //Buy two
        return true;
      }

    }
    else{
      throw new RuntimeException("Company don't exists");
    }


    return false;
  }

}
